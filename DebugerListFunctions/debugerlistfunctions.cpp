#include "debugerlistfunctions.h"
#include "ui_debugerlistfunctions.h"

#include "analizeframestackgdb.h"

#include <event.h>

#include <QMenu>

DebugerListFunctions::DebugerListFunctions(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugerListFunctions)
{
    ui->setupUi(this);

    qRegisterMetaType<pBear::debuger::Frame>("pBear::debuger::Frame");
    qRegisterMetaType<std::list<pBear::debuger::Frame>>("std::list<pBear::debuger::Frame>");
    qRegisterMetaType<std::shared_ptr<pBear::ConnectionClient::Process>>("std::shared_ptr<pBear::ConnectionClient::Process>");

    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(std::bind(&DebugerListFunctions::start, this, std::placeholders::_1, std::placeholders::_2));
    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(std::bind(&DebugerListFunctions::end, this, std::placeholders::_1, std::placeholders::_2));

    connect(ui->treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*,int)));

    connect(this, SIGNAL(signallCurrFrameEvent(pBear::debuger::Frame)), this, SLOT(slotCurrFrameEvent(pBear::debuger::Frame)));
    connect(this, SIGNAL(signallListEvent(std::list<pBear::debuger::Frame>)), this, SLOT(slotListEvent(std::list<pBear::debuger::Frame>)));
    connect(this, SIGNAL(signalQtEnd(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)),
            SLOT(slotQtEnd(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)));
}

DebugerListFunctions::~DebugerListFunctions()
{
    delete ui;
}

void DebugerListFunctions::start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (!this->pro.get())
    {
        this->pro = pro;
        this->deb = deb;
        deb->getControler()->getAnalizer<pBear::debuger::AnalizeFrameStackGDB>()->addFunctionAll(std::bind(&DebugerListFunctions::listEvent, this, std::placeholders::_1));
        deb->getControler()->getAnalizer<pBear::debuger::AnalizeFrameStackGDB>()->addFunctionCurr(std::bind(&DebugerListFunctions::currFrameEvent, this, std::placeholders::_1));
    }
}

void DebugerListFunctions::end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    emit signalQtEnd(pro, deb);
}

void DebugerListFunctions::listEvent(std::list<pBear::debuger::Frame> frames)
{
    emit signallListEvent(frames);
}

void DebugerListFunctions::currFrameEvent(pBear::debuger::Frame frame)
{
    emit signallCurrFrameEvent(frame);
}

void DebugerListFunctions::itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    deb->getControler()->stackSelectFrame(item->data(3, 0).value<pBear::debuger::Frame>().level);
}

void DebugerListFunctions::slotListEvent(std::list<pBear::debuger::Frame> frames)
{
    ui->treeWidget->clear();
    for (pBear::debuger::Frame frame: frames)
    {
        QTreeWidgetItem *item = new QTreeWidgetItem;
        item->setText(0, frame.func.c_str());
        item->setText(1, frame.file.c_str());
        item->setText(2, std::to_string(frame.line + 1).c_str());
        item->setData(3, 0, QVariant::fromValue<pBear::debuger::Frame>(frame));

        /*for (int i = 0; i < 3; i++)
        {
            item->setBackgroundColor(i, QColor::blue());
        }*/
        ui->treeWidget->addTopLevelItem(item);
    }
}

void DebugerListFunctions::slotCurrFrameEvent(pBear::debuger::Frame frame)
{
    std::cout << "........................................." << std::endl;
    for (int i = 0; i < 3; i++)
    {
        ui->treeWidget->topLevelItem(frame.level)->setBackgroundColor(i, "#bba2f3");
    }
}

void DebugerListFunctions::slotQtEnd(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (pro == this->pro)
    {
        deb = nullptr;
        pro.reset();
        ui->treeWidget->clear();
    }
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new DebugerListFunctions;
    }
    std::string type()
    {
        return "QDockWidget";
    }
    std::string orientation()
    {
        return "bottom";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        return new QMenu("List Functions");
    }
}

