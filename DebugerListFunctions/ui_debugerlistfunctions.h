/********************************************************************************
** Form generated from reading UI file 'debugerlistfunctions.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGERLISTFUNCTIONS_H
#define UI_DEBUGERLISTFUNCTIONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugerListFunctions
{
public:
    QVBoxLayout *verticalLayout;
    QTreeWidget *treeWidget;

    void setupUi(QWidget *DebugerListFunctions)
    {
        if (DebugerListFunctions->objectName().isEmpty())
            DebugerListFunctions->setObjectName(QStringLiteral("DebugerListFunctions"));
        DebugerListFunctions->resize(400, 300);
        verticalLayout = new QVBoxLayout(DebugerListFunctions);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        treeWidget = new QTreeWidget(DebugerListFunctions);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));

        verticalLayout->addWidget(treeWidget);


        retranslateUi(DebugerListFunctions);

        QMetaObject::connectSlotsByName(DebugerListFunctions);
    } // setupUi

    void retranslateUi(QWidget *DebugerListFunctions)
    {
        DebugerListFunctions->setWindowTitle(QApplication::translate("DebugerListFunctions", "DebugerListFunctions", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(2, QApplication::translate("DebugerListFunctions", "Line", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("DebugerListFunctions", "File name", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("DebugerListFunctions", "Function", 0));
    } // retranslateUi

};

namespace Ui {
    class DebugerListFunctions: public Ui_DebugerListFunctions {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGERLISTFUNCTIONS_H
