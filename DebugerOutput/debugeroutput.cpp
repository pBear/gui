#include "debugeroutput.h"
#include "ui_debugeroutput.h"
#include "executedebugcompileterminal.h"
#include "terminalprocessconnectordebuger.h"
#include "analizerstopgdb.h"
#include "AnalizerGDB.h"
#include "analizervariablesgdb.h"
#include "AnalizeFrameStack.h"

#include <functional>

using namespace pBear;

DebugerOutput::DebugerOutput(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugerOutput)
{
    ui->setupUi(this);

    qRegisterMetaType<pBear::debuger::StopReason>();
    qRegisterMetaType<std::list<std::string>>();
    qRegisterMetaType<pBear::debuger::VarData>();
    qRegisterMetaType<std::list<pBear::debuger::VarChange>>();

    //pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(std::bind(&DebugerOutput::start, this, std::placeholders::_1, std::placeholders::_2));
    //pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(std::bind(&DebugerOutput::end, this, std::placeholders::_1));

    //void addNewDebuger(std::function<void (std::shared_ptr<ConnectionClient::Process>, std::shared_ptr<ConnectionClient::ProcessQueneElement_Interface>)> fun);
    connect(this, SIGNAL(stopHandleQTSignal(pBear::debuger::StopReason)), this, SLOT(stopHandlerQT(pBear::debuger::StopReason)));
    connect(this, SIGNAL(localsHandleQtSignal(std::list<std::string>)), this, SLOT(localsHandleQT(std::list<std::string>)));
    connect(this, SIGNAL(variablesHandleQTSignal(pBear::debuger::VarData)), this, SLOT(variablesHandlerQT(pBear::debuger::VarData)));
    connect(this, SIGNAL(variablesChangeHandlerQTSignal(std::list<pBear::debuger::VarChange>)), this, SLOT(variablesChangeHandlerQT(std::list<pBear::debuger::VarChange>)));
    connect(ui->treeWidget, SIGNAL(itemChanged(QTreeWidgetItem*,int)), this, SLOT(itemChangedHandlerQT(QTreeWidgetItem*,int)));
    connect(ui->treeWidget, SIGNAL(itemExpanded(QTreeWidgetItem*)), this, SLOT(itemExpandedHandlerQT(QTreeWidgetItem*)));

}

void DebugerOutput::start(std::shared_ptr<ConnectionClient::Process> pro, debuger::TerminalProcessConnectorDebuger* deb)
{
    if (!this->pro.get())
    {
        this->pro = pro;
        this->deb = deb;
        //deb->getControler()->getAnalizers();

        deb->getControler()->getAnalizer<pBear::debuger::AnalizerStopGDB>()->addEventFunction(std::bind(&DebugerOutput::stopHandler, this, std::placeholders::_1));
        deb->getControler()->getAnalizer<pBear::debuger::AnalizerVariablesGDB>()->addFunLocals(std::bind(&DebugerOutput::localsHandler, this, std::placeholders::_1));
        deb->getControler()->getAnalizer<pBear::debuger::AnalizerVariablesGDB>()->addFunVar(std::bind(&DebugerOutput::variableHandler, this, std::placeholders::_1));
        deb->getControler()->getAnalizer<pBear::debuger::AnalizerVariablesGDB>()->addFunChangeVars(std::bind(&DebugerOutput::variablesChangeHandler, this, std::placeholders::_1));

        deb->getControler()->getAnalizer<pBear::debuger::AnalizeFrameStack>()->addFunctionCurr(std::bind(&DebugerOutput::currFrameEvent, this, std::placeholders::_1));

        deb->getControler()->setBreakpoint("/home/aron/main.cpp", 7);
        deb->getControler()->run();
        sleep(1);
        //deb->getControler()->stackListVariables();
    }
}

void DebugerOutput::end(std::shared_ptr<ConnectionClient::Process> pro)
{
    if (this->pro = pro)
    {
        this->pro.reset();
    }
}

void DebugerOutput::stopHandler(debuger::StopReason sr)
{
    emit stopHandleQTSignal(sr);
}

void DebugerOutput::localsHandler(std::list<std::string> locals)
{
    emit localsHandleQtSignal(locals);
}

void DebugerOutput::variableHandler(debuger::VarData vd)
{
    emit variablesHandleQTSignal(vd);
}

void DebugerOutput::variablesChangeHandler(std::list<debuger::VarChange> vch)
{
    emit variablesChangeHandlerQTSignal(vch);
}

void DebugerOutput::variablesNumHandle(int num)
{
    /*DebugerItem *tmp = nullptr;
    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
    {
        DebugerItem *tmp1;
        if (tmp1 = findItem(varTmp, dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i))))
        {
            tmp = tmp1;
            break;
        }
    }
    if (tmp)
    {
        pBear::debuger::VarData data = tmp->getData();

    }*/
}

void DebugerOutput::currFrameEvent(debuger::Frame fr)
{
    this->fr  = fr;
    DebugerItem *top = nullptr;
    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
    {
        DebugerItem *item = dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i));
        if (item->isTopLevel() && item->getFrame().level == fr.level)
        {
            top = item;
        }
    }
    if (!top)
    {
        top = new DebugerItem;
        top->setTopLevel(true);
        top->setFrame(fr);
        top->setText(0, ("Frame: " +  std::to_string(fr.level)).c_str());
        ui->treeWidget->addTopLevelItem(top);
    }
    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
    {
        DebugerItem *item = dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i));
        if (!item->isLoaded() && !item->isTopLevel())
        {
            item->setFrame(fr);
            top->addChild(ui->treeWidget->takeTopLevelItem(i));
        }
    }
    deb->getControler()->stackListVariables(fr.level);
}

debuger::TerminalProcessConnectorDebuger *DebugerOutput::getDeb()
{
    return deb;
}

DebugerOutput::~DebugerOutput()
{
    delete ui;
}

void DebugerOutput::stopHandlerQT(debuger::StopReason sr)
{
    std::string type_str;
    switch (sr.type)
    {
    case debuger::StopReason::ERROR:
        type_str = "ERROR";
        break;
    case debuger::StopReason::EXITED_NORMALLY:
        type_str = "EXITED_NORMALLY";
        break;
    case debuger::StopReason::END_STEPPING_RANGE:
        type_str = "END_STEPPING_RANGE";
        break;
    case debuger::StopReason::UNDEFINED:
        type_str = "UNDEFINED";
        break;
    case debuger::StopReason::BRAKPOINT_HIT:
        type_str = "BRAKPOINT_HIT";
        break;
    }

    ui->label->setText(type_str.c_str());
}

void DebugerOutput::localsHandleQT(std::list<std::string> vars)
{
    //parentItem->appendRow(new QStandardItem("item"));
    for (std::string str: vars)
    {
        //DebugerItem *tmp = new DebugerItem(str);
        //ui->treeWidget->addTopLevelItem(tmp);
        //deb->getControler()->stackSelectFrame(0);
        deb->getControler()->varCreate(str, str + std::to_string(fr.level));
    }
}

DebugerItem *findItem(std::string name, DebugerItem *it)
{
    if (it->getName() == name && !it->isTopLevel())
    {
        return it;
    }
    for (int i = 0; i < it->childCount(); i++)
    {
        return findItem(name, dynamic_cast<DebugerItem*>(it->child(i)));
    }
    return nullptr;
}

void DebugerOutput::variablesHandlerQT(debuger::VarData vd)
{
    std::cout << "+++++++++++++++++++++++++++++1" << std::endl;
    DebugerItem *tmp = nullptr;
    for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
    {
        DebugerItem *tmp1;
        if ((tmp1 = findItem(vd.parent, dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i)))))
        {
            tmp = tmp1;
            break;
        }
    }
    std::cout << "+++++++++++++++++++++++++++++2" << std::endl;
    DebugerItem *tmpItem = new DebugerItem(vd.name);
    if (!tmp)
    {
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
        {
            if (ui->treeWidget->topLevelItem(i)->text(0).isEmpty())
            {
                delete ui->treeWidget->topLevelItem(i);
                break;
            }
        }
        std::cout << "+++++++++++++++++++++++++++++2.1" << std::endl;
        DebugerItem *top = nullptr;
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
        {
            DebugerItem *item = dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i));
            if (item->isTopLevel() && item->getFrame().level == fr.level)
            {
                top = item;
            }
        }
        std::cout << "Curr Frame: " << fr.level << std::endl;
        if (top)
        {
            top->addChild(tmpItem);
        }
        else
        {
            ui->treeWidget->addTopLevelItem(tmpItem);
        }
    }
    else
    {
        for (int i = 0; i < tmp->childCount(); i++)
        {
            if (tmp->child(i)->text(0).isEmpty())
            {
                delete tmp->child(i);
                break;
            }
        }
        tmp->addChild(tmpItem);
    }
    std::cout << "+++++++++++++++++++++++++++++3" << std::endl;
    tmpItem->setData(vd);
    //deb->getControler()->listChilds(vd.name);
}

void DebugerOutput::variablesChangeHandlerQT(std::list<debuger::VarChange> vches)
{
    for (debuger::VarChange vch: vches)
    {
        DebugerItem *tmp = nullptr;
        for (int i = 0; i < ui->treeWidget->topLevelItemCount(); i++)
        {
            DebugerItem *tmp1;
            if (tmp1 = findItem(vch.name, dynamic_cast<DebugerItem*>(ui->treeWidget->topLevelItem(i))))
            {
                tmp = tmp1;
                break;
            }
        }
        if (tmp)
        {
            if (!vch.in_scape)
            {
                delete tmp;
            }
            else
            {
                pBear::debuger::VarData data = tmp->getData();
                data.value = vch.value;
                tmp->setData(data);
                //deb->getControler()->listChilds(data.name);
            }
        }
    }
}

void DebugerOutput::itemExpandedHandlerQT(QTreeWidgetItem *item)
{
    DebugerItem *tmp = dynamic_cast<DebugerItem*>(item);
    if (tmp->isTopLevel()) return;
    if (tmp && !tmp->getChildsLoaded())
    {
        deb->getControler()->listChilds(tmp->getData().name);
        tmp->setChildLoaded(true);
    }
}

void DebugerOutput::itemChangedHandlerQT(QTreeWidgetItem *item, int column)
{
    if (column == 1)
    {
        DebugerItem *tmp = dynamic_cast<DebugerItem*>(item);
        if (tmp->isTopLevel()) return;
        deb->getControler()->varAssign(tmp->getData().name, item->text(1).toStdString());
        deb->getControler()->varUpdate();
    }
}


