#ifndef DEBUGEROUTPUT_H
#define DEBUGEROUTPUT_H

#include "model.h"
#include "executedebugcompileterminal.h"
#include "analizervariablesgdb.h"

#include <memory>
#include <regex>

#include <QWidget>
#include <QTreeWidgetItem>

namespace Ui {
class DebugerOutput;
}

Q_DECLARE_METATYPE(pBear::debuger::StopReason)
Q_DECLARE_METATYPE(std::list<std::string>)
Q_DECLARE_METATYPE(pBear::debuger::VarData)
Q_DECLARE_METATYPE(std::list<pBear::debuger::VarChange>)

class DebugerItem: public QTreeWidgetItem
{
private:
    pBear::debuger::VarData data;
    std::string name;
    bool        childLoaded = false;
    pBear::debuger::Frame fr;
    bool                  loaded  = false;
    bool                  topLevel = false;
public:
    DebugerItem() = default;
    DebugerItem(std::string name)
    {
        this->name = name;
    }

    pBear::debuger::VarData getData()
    {
        return data;
    }

    void                    setData( pBear::debuger::VarData data)
    {
        this->data = data;
        if (!data.exp.empty())
        {
            setText(0, data.exp.c_str());
        }
        else
        {
            setText(0, (name + " (the number is the frame)").c_str());
        }
        setText(1, data.value.c_str());
        setText(2, data.type.c_str());
        if (std::stoi(data.numChilds))
        {
            addChild(new DebugerItem(""));
        }
        setFlags(flags() | Qt::ItemIsEditable);

    }
    std::string             getName()
    {
        return name;
    }
    bool getChildsLoaded()
    {
        return childLoaded;
    }
    void setChildLoaded(bool state)
    {
        childLoaded = state;
    }
    void setFrame(pBear::debuger::Frame fr)
    {
        this->fr = fr;
        loaded = true;
    }
    bool isLoaded()
    {
        return loaded;
    }
    void setTopLevel(bool topLevel)
    {
        this->topLevel = topLevel;
    }
    bool isTopLevel()
    {
        return topLevel;
    }
    pBear::debuger::Frame getFrame()
    {
        return fr;
    }
};

class DebugerOutput : public QWidget
{
    Q_OBJECT
private:
    std::shared_ptr<pBear::ConnectionClient::Process> pro;
    pBear::debuger::TerminalProcessConnectorDebuger  *deb;
    std::string                                       varTmp;
    pBear::debuger::Frame                                             fr;
public:
    explicit DebugerOutput(QWidget *parent = 0);
    void start(std::shared_ptr<pBear::ConnectionClient::Process>, pBear::debuger::TerminalProcessConnectorDebuger *deb);
    void end(std::shared_ptr<pBear::ConnectionClient::Process>);
    void stopHandler(pBear::debuger::StopReason sr);
    void localsHandler(std::list<std::string> locals);
    void variableHandler(pBear::debuger::VarData vd);
    void variablesChangeHandler(std::list<pBear::debuger::VarChange> vch);
    void variablesNumHandle(int);
    void currFrameEvent(pBear::debuger::Frame fr);
    pBear::debuger::TerminalProcessConnectorDebuger *getDeb();
    ~DebugerOutput();
private slots:
    void stopHandlerQT(pBear::debuger::StopReason sr);
    void localsHandleQT(std::list<std::string> vars);
    void variablesHandlerQT(pBear::debuger::VarData vd);
    void variablesChangeHandlerQT(std::list<pBear::debuger::VarChange> vch);
    void itemExpandedHandlerQT(QTreeWidgetItem * item);
    void itemChangedHandlerQT(QTreeWidgetItem * item, int column);
signals:
    void stopHandleQTSignal(pBear::debuger::StopReason);
    void localsHandleQtSignal(std::list<std::string>);
    void variablesHandleQTSignal(pBear::debuger::VarData vd);
    void variablesChangeHandlerQTSignal(std::list<pBear::debuger::VarChange>);
private:
    Ui::DebugerOutput *ui;
};

#endif // DEBUGEROUTPUT_H
