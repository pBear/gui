/********************************************************************************
** Form generated from reading UI file 'debugeroutputtab.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGEROUTPUTTAB_H
#define UI_DEBUGEROUTPUTTAB_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugerOutputTab
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;

    void setupUi(QWidget *DebugerOutputTab)
    {
        if (DebugerOutputTab->objectName().isEmpty())
            DebugerOutputTab->setObjectName(QStringLiteral("DebugerOutputTab"));
        DebugerOutputTab->resize(400, 300);
        verticalLayout = new QVBoxLayout(DebugerOutputTab);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(DebugerOutputTab);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));

        verticalLayout->addWidget(tabWidget);


        retranslateUi(DebugerOutputTab);

        QMetaObject::connectSlotsByName(DebugerOutputTab);
    } // setupUi

    void retranslateUi(QWidget *DebugerOutputTab)
    {
        DebugerOutputTab->setWindowTitle(QApplication::translate("DebugerOutputTab", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class DebugerOutputTab: public Ui_DebugerOutputTab {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGEROUTPUTTAB_H
