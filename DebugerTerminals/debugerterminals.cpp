#include "debugerterminals.h"
#include "ui_debugerterminals.h"

#include "terminalprocessconnectordebuger.h"

#include "debugerterminal.h"

#include <iostream>

#include <QMenu>

using namespace pBear;

DebugerTerminals::DebugerTerminals(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DebugerTerminals)
{
    ui->setupUi(this);

    qRegisterMetaType<std::shared_ptr<pBear::ConnectionClient::Process>>();
    qRegisterMetaType<pBear::debuger::TerminalProcessConnectorDebuger*>();

    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(std::bind(&DebugerTerminals::addTerminal, this, std::placeholders::_1, std::placeholders::_2));
    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(std::bind(&DebugerTerminals::closeTerminal, this, std::placeholders::_1, std::placeholders::_2));

    connect(this, SIGNAL(addTerminalQTSignal(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)), this,
            SLOT(addTerminalQT(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)));
    connect(this, SIGNAL(closeTerminalQtSignal(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)), this,
            SLOT(closeTerminalQt(std::shared_ptr<pBear::ConnectionClient::Process>,pBear::debuger::TerminalProcessConnectorDebuger*)));
}

void DebugerTerminals::addTerminal(std::shared_ptr<ConnectionClient::Process> pro, debuger::TerminalProcessConnectorDebuger *tcd)
{
    emit addTerminalQTSignal(pro, tcd);
}

void DebugerTerminals::closeTerminal(std::shared_ptr<ConnectionClient::Process> pro, debuger::TerminalProcessConnectorDebuger *deb)
{
    emit closeTerminalQtSignal(pro, deb);
}

void DebugerTerminals::addTerminalQT(std::shared_ptr<ConnectionClient::Process> pro, debuger::TerminalProcessConnectorDebuger *tcd)
{
    ui->tabWidget->addTab(new DebugerTerminal(pro, tcd), "tab");
}

void DebugerTerminals::closeTerminalQt(std::shared_ptr<ConnectionClient::Process> pro, debuger::TerminalProcessConnectorDebuger *)
{
    for (int i = 0; i < ui->tabWidget->count(); i++)
    {
        if (dynamic_cast<DebugerTerminal*>(ui->tabWidget->widget(i))->getProcess() == pro)
        {
            ui->tabWidget->removeTab(i);
        }
    }
}

DebugerTerminals::~DebugerTerminals()
{
    delete ui;
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new DebugerTerminals;
    }
    std::string type()
    {
        return "QDockWidget";
    }
    std::string orientation()
    {
        return "bottom";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        return new QMenu("Debuger Terminals");
    }
}

