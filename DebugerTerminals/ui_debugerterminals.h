/********************************************************************************
** Form generated from reading UI file 'debugerterminals.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEBUGERTERMINALS_H
#define UI_DEBUGERTERMINALS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DebugerTerminals
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;

    void setupUi(QWidget *DebugerTerminals)
    {
        if (DebugerTerminals->objectName().isEmpty())
            DebugerTerminals->setObjectName(QStringLiteral("DebugerTerminals"));
        DebugerTerminals->resize(814, 328);
        verticalLayout = new QVBoxLayout(DebugerTerminals);
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(DebugerTerminals);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));

        verticalLayout->addWidget(tabWidget);


        retranslateUi(DebugerTerminals);

        QMetaObject::connectSlotsByName(DebugerTerminals);
    } // setupUi

    void retranslateUi(QWidget *DebugerTerminals)
    {
        DebugerTerminals->setWindowTitle(QApplication::translate("DebugerTerminals", "Form", 0));
    } // retranslateUi

};

namespace Ui {
    class DebugerTerminals: public Ui_DebugerTerminals {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEBUGERTERMINALS_H
