#ifndef CHOSEPROJECTNAME_H
#define CHOSEPROJECTNAME_H

#include <QDialog>

namespace Ui {
class ChoseProjectName;
}

class ChoseProjectName : public QDialog
{
    Q_OBJECT

public:
    explicit ChoseProjectName(QWidget *parent = 0);
    ~ChoseProjectName();

private:
    Ui::ChoseProjectName *ui;
};

#endif // CHOSEPROJECTNAME_H
