#ifndef EDITPROYECT_H
#define EDITPROYECT_H

#include "project.h"
#include "event.h"

#include <memory>

#include <QDialog>

namespace Ui {
class editProyect;
}

namespace pBear
{
class editProyect : public QDialog
{
    Q_OBJECT
private:
    Ui::editProyect *ui;
    //corre::Project   pro;
    corre::Event    *event;
    static editProyect *once;

    void setComboCompileExecuteCompileMode();
public:
    explicit editProyect(QWidget *parent = 0);
    ~editProyect();
public slots:
    static editProyect *instance();
    void show(corre::Event* event, std::string language, QWidget *widget);
    void search();
    void accept();
    void currentIndexChanged(int index);
    void currentIndexCompileExecuteCompileChanged(int index);
    void changeProName(QString str);
};
}

#endif // EDITPROYECT_H
