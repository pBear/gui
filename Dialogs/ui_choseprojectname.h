/********************************************************************************
** Form generated from reading UI file 'choseprojectname.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CHOSEPROJECTNAME_H
#define UI_CHOSEPROJECTNAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ChoseProjectName
{
public:
    QVBoxLayout *verticalLayout;
    QLineEdit *lineEdit;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ChoseProjectName)
    {
        if (ChoseProjectName->objectName().isEmpty())
            ChoseProjectName->setObjectName(QStringLiteral("ChoseProjectName"));
        ChoseProjectName->resize(400, 101);
        verticalLayout = new QVBoxLayout(ChoseProjectName);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        lineEdit = new QLineEdit(ChoseProjectName);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        label = new QLabel(ChoseProjectName);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        buttonBox = new QDialogButtonBox(ChoseProjectName);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(ChoseProjectName);
        QObject::connect(buttonBox, SIGNAL(accepted()), ChoseProjectName, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ChoseProjectName, SLOT(reject()));

        QMetaObject::connectSlotsByName(ChoseProjectName);
    } // setupUi

    void retranslateUi(QDialog *ChoseProjectName)
    {
        ChoseProjectName->setWindowTitle(QApplication::translate("ChoseProjectName", "Dialog", 0));
        label->setText(QApplication::translate("ChoseProjectName", "TextLabel", 0));
    } // retranslateUi

};

namespace Ui {
    class ChoseProjectName: public Ui_ChoseProjectName {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CHOSEPROJECTNAME_H
