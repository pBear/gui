#include "verticalbutton.h"

#include <iostream>
#include <functional>

verticalButtonBox::verticalButtonBox(bool horizontal, QWidget *parent) :
    QWidget(parent), horizontal(horizontal)
{
    if (!horizontal)
    {
        lay = new QVBoxLayout;
    }
    else
    {
        lay = new QHBoxLayout;
    }
    lay->setMargin(0);
    //setFixedWidth(300);
    setLayout(lay);
    enableHideOnClick = false;
    invert            = false;
}

void     verticalButtonBox::addButton(QString name, QWidget *widget)
{
    OrientationButton *but = new OrientationButton(name, this);

    but->setCheckable(true);
    but->setFlat(true);
    if (!horizontal)
    {
        but->setOrientation(Qt::Vertical);
    }
    else
    {
        but->setOrientation(Qt::Horizontal);
    }
    but->setMirrored(!invert);
    lay->addWidget(but);
    connect(but, SIGNAL(released()), this, SLOT(released()));
    wids.push_back(widget);
    buts.push_back(but);
    but->setHideClickEnable(enableHideOnClick);
    if (widget->isVisible())
    {
        but->setChecked(true);
    }
    else
    {
        but->setChecked(false);
    }
    connect(dynamic_cast<QDockWidget*>(widget), &QDockWidget::visibilityChanged, std::bind(&verticalButtonBox::hideQDockWidget, this, dynamic_cast<QDockWidget*>(widget), std::placeholders::_1));
    connect(dynamic_cast<QDockWidget*>(widget), &QDockWidget::topLevelChanged, std::bind(&verticalButtonBox::topLevelChangedQDockWidget, this, dynamic_cast<QDockWidget*>(widget), std::placeholders::_1));
}

void verticalButtonBox::removeButton(QWidget *widget)
{
    if (!hasButton(widget)) return;
    dynamic_cast<QDockWidget*>(widget)->disconnect();
    auto iter = std::find(wids.begin(), wids.end(), widget);
    QWidget *but = buts[std::distance(wids.begin(), iter)];

    wids.erase(std::find(wids.begin(), wids.end(), widget));
    buts.erase(std::find(buts.begin(), buts.end(), but));


    lay->removeWidget(but);
    delete but;
}

void verticalButtonBox::released()
{
    QObject           *sen = sender();
    for (unsigned i = 0, n = lay->count(); i < n; i++)
    {
        OrientationButton *tmp = dynamic_cast<OrientationButton*>(lay->itemAt(i)->widget());
        if (sen == tmp)
        {
            if (!tmp->isChecked())
            {
                wids[i]->hide();
            }
            else
            {
                wids[i]->show();
            }
        }
    }
}

void verticalButtonBox::changeActiveButton(unsigned j)
{
    OrientationButton *sen = dynamic_cast<OrientationButton*>(lay->itemAt(j)->widget());
    for (unsigned i = 0, n = lay->count(); i < n; i++)
    {
        OrientationButton *tmp = dynamic_cast<OrientationButton*>(lay->itemAt(i)->widget());
        if (sen != tmp)
        {
            sen->setChecked(!sen->isChecked());
            wids[i]->hide();
        }
    }
}
void slotHide(QWidget* wid);

void verticalButtonBox::changeActiveWidget(unsigned j)
{
    QWidget *wi = wids[j];
    std::for_each(wids.begin(), wids.end(), [&wi](QWidget* wid)
    {
        if (wi != wid)
        {
            wid->hide();
        }
        else
        {
            wid->show();
            wid->parentWidget()->show();
        }
    });
}

void verticalButtonBox::setHideClickEnable(bool stat)
{
    enableHideOnClick = stat;
    for (unsigned i = 0, n = lay->count(); i < n; i++)
    {
        OrientationButton *tmp = dynamic_cast<OrientationButton*>(lay->itemAt(i)->widget());
        tmp->setHideClickEnable(stat);
    }
}

bool verticalButtonBox::hasButton(QWidget *wid)
{
    return std::find(wids.begin(), wids.end(), wid) != wids.end();
}

void verticalButtonBox::postConigureHide()
{
    for (int i = 0; i < wids.size(); i++)
    {
        if (wids[i]->isVisible())
        {
            buts[i]->setChecked(true);
        }
        else
        {
            buts[i]->setChecked(false);
        }
    }
}

void verticalButtonBox::hideAll()
{
    std::for_each(wids.begin(), wids.end(), [](QWidget* wid)
    {
        wid->hide();
        //wid->parentWidget()->hide();
    });
    //parentWidget()->setSizeH
}

void verticalButtonBox::setInvert(bool state)
{
    invert = state;
    OrientationButton *tmp = NULL;
    for (unsigned i = 0, n = lay->count(); i < n; i++)
    {
        tmp->setMirrored(!state);
    }
}

void verticalButtonBox::hideQDockWidget(QDockWidget *widget, bool state)
{
    for (int i = 0; i < wids.size(); i++)
    {
        if (wids[i]->isVisible())
        {
            buts[i]->setChecked(true);
        }
        else
        {
            buts[i]->setChecked(false);
        }
    }
}

void verticalButtonBox::topLevelChangedQDockWidget(QDockWidget *widget, bool topLevel)
{
    OrientationButton *buttom = buts[std::distance(wids.begin(), std::find(wids.begin(), wids.end(), widget))];
    if (topLevel)
    {
        buttom->hide();
    }
    else
    {
        buttom->show();
    }
}
