#ifndef VERTICALPUSHBUTTON_H
#define VERTICALPUSHBUTTON_H

#include <QPushButton>
#include <QStyleOptionButton>

class OrientationButton : public QPushButton
{
public:
    OrientationButton(QWidget* parent = 0);
    OrientationButton(const QString& text, QWidget* parent = 0);
    OrientationButton(const QIcon& icon, const QString& text, QWidget* parent = 0);
    ~OrientationButton();

    Qt::Orientation orientation() const;
    void setOrientation(Qt::Orientation orientation);

    bool mirrored() const;
    void setMirrored(bool mirrored);

    void setHideClickEnable(bool stat);

    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent* event);
    void mousePressEvent(QMouseEvent *e);

private:
    QStyleOptionButton getStyleOption() const;
    void init();

    Qt::Orientation orientation_;
    bool mirrored_;
    bool enableHideOnClick;
};

#endif VERTICALPUSHBUTTON_H
