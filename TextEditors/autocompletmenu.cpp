#include "autocompletmenu.h"
#include <QMouseEvent>

using namespace pBear::gui;
using namespace pBear::corre;


void AutocompleteMenu::focusOutEvent(QFocusEvent *event)
{
    hide();
    QListWidget::focusOutEvent(event);
}

void AutocompleteMenu::insertText()
{
    std::string str = doc.getAnalizer()->getApropiatedInsertText(edit->textCursor().position(),
                                               currentItem()->text().toStdString());
    edit->textCursor().insertText(str.c_str());
    hide();
}

AutocompleteMenu::AutocompleteMenu(QPlainTextEdit *edit, QWidget *parent):
    QListWidget(parent)
{
    this->edit = edit;
    setWindowFlags( Qt::CustomizeWindowHint|Qt::Dialog);
}

void AutocompleteMenu::setDoc(Document doc)
{
    this->doc = doc;
}

void AutocompleteMenu::exec()
{
    QTextCursor cur = edit->textCursor();
    clear();
    auto data = doc.getAnalizer()->getCompUseful(cur.blockNumber(), cur.columnNumber(), cur.position());
    comps = data.first;
    move(edit->mapToGlobal(edit->cursorRect(edit->textCursor()).bottomLeft()));
    for (corre::compData x: comps)
    {
        addItem(x.name.c_str());
    }
    show();
    setFocus();
    setCurrentRow(0);
}

#include <iostream>

void AutocompleteMenu::keyPressEvent(QKeyEvent *event)
{
    switch (event->key())
    {
    case Qt::Key_Return:
        insertText();
        break;
    case Qt::Key_Down:
    case Qt::Key_Up:
        QListWidget::keyPressEvent(event);
        break;
    case Qt::Key_Right:
        hide();
        emit keyPress(event);
        break;
    case Qt::Key_Left:
        hide();
        emit keyPress(event);
        break;
    default:
        emit keyPress(event);
        break;
    };
}

void AutocompleteMenu::keyReleaseEvent(QKeyEvent *event)
{
    emit keyRell(event);
}

void AutocompleteMenu::itemChanged(QListWidgetItem *, QListWidgetItem *)
{

}

void AutocompleteMenu::mouseDoubleClickEvent(QMouseEvent *)
{
    insertText();
}
