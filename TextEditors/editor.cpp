#include "editor.h"
#include "language.h"
#include "ui_editor.h"
#include "project.h"
#include "line.h"
#include "editorwidget.h"

#include <QScrollBar>

using namespace pBear::gui;
using namespace pBear::corre;

#include <iostream>

void Editor::upLineData(int)
{
    std::vector<unsigned> lines;
    std::vector<unsigned> top;
    std::vector<unsigned> booton;

    QPlainTextEdit * text = ui->textEdit;

    QTextCursor cursor    = text->cursorForPosition
            (text->frameRect().topLeft());
    QTextCursor end       = text->cursorForPosition
            (text->frameRect().bottomLeft());
    unsigned    index     = cursor.blockNumber();
    unsigned    end_n     = end.blockNumber() + 1;
    QRect       textRec;

    for (; index < end_n; index++)
    {
        if (cursor.block().isVisible())
        {
            textRec = text->cursorRect(cursor);
            textRec.setLeft(10);
            textRec.setRight(20);
            lines.push_back(cursor.block().blockNumber());
            top.push_back(textRec.top());
            booton.push_back(textRec.bottom());
        }
        cursor.movePosition(QTextCursor::NextBlock);
    }

    ui->widget->repaint(lines, top, booton);
}

void Editor::repaintPlugins()
{
    ui->widget->repaintPlugins();
}

void Editor::restartTimer()
{
    time.start(100);
}

void Editor::repaintLocal()
{
    //ui->textEdit->repaint();
    emit restartTimerSignal();
}

void Editor::hideSeeBlock(Document::iterator it, bool hide)
{
    ui->textEdit->document()->findBlockByNumber(it->getLine()).setVisible(!hide);
    repaintLocal();
    ui->textEdit->viewport()->update();
}

void Editor::paintEvent(QPaintEvent *event)
{
    QDialog::paintEvent(event);
}



Editor::Editor(corre::Document doc, QWidget *parent) :
    QDialog(parent), ui(new Ui::Editor), doc(doc)
{
    ui->setupUi(this);

    ui->widget->setDoc(doc);

    connect(dynamic_cast<EditorWidget*>(parent), SIGNAL(signalRepaintPlugins()), this, SLOT(repaintPlugins()));

    doc.addFunctionAna(std::bind(&pBear::gui::Editor::repaintLocal, this), this);

    if (doc.getText().empty()) doc.read();

    ui->textEdit->setPlainText(doc.getText().c_str());
    connect(ui->textEdit->document(),
            SIGNAL(contentsChange(int,int,int)),
            this, SLOT(contentsChange(int,int,int)));

    col = std::make_shared<ColorizeText>(ui->textEdit->document(), doc);
    des = false;

    connect(ui->textEdit->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(upLineData(int)));
    connect(this, SIGNAL(restartTimerSignal()), this, SLOT(restartTimer()));

    time.setSingleShot(true);

    connect(&time, SIGNAL(timeout()), this, SLOT(upLineData()));

    //std::bind(&pBear::gui::Editor::hideSeeBlock, this, std::placeholders::_1, std::placeholders::_2);
    doc.addFunctionHide(std::bind(&pBear::gui::Editor::hideSeeBlock, this, std::placeholders::_1, std::placeholders::_2), this);

    ui->textEdit->setDocument(doc);

    time.start(100);

    ui->widget->setTextEditWidget(ui->textEdit);

}

Editor::~Editor()
{
    des = true;
    delete ui;
}


pBear::corre::Document& Editor::getDoc()
{
    return doc;
}

QPlainTextEdit *Editor::getTextEdit()
{
    return ui->textEdit;
}

void Editor::updateLanguage()
{
    des = true;
    ui->textEdit->setPlainText(doc.getText().c_str());
    restartTimer();
    des = false;
}

void Editor::contentsChange(int position, int charsRemoved, int charsAdded)
{
    if (des) return;


    int befor = std::distance(doc.begin(), doc.end());

    doc.setText(position, charsRemoved,
                charsAdded, ui->textEdit->toPlainText().toStdString());

    int after = std::distance(doc.begin(), doc.end());


    if (befor != after) time.start(100);
}

void Editor::execAutocompletMenu()
{
    /*if (autocomplet)
    {
        autocomplet = std::make_shared<AutocompleteMenu>(doc, ui->textEdit, ui->textEdit);
    }
    autocomplet->exec();*/
}


void ColorizeText::setColor(int index, int length, std::string color)
{
    QTextCharFormat tmp;
    tmp.setForeground(QBrush(QColor(color.c_str())));
    setFormat(index, length, tmp);
}

void ColorizeText::highlightBlock(const QString &text)
{
    if (doc.getProyect().getLanguage().empty()) return;

    corre::LanguageDatas *lan = Language::instance()->getLanguage(doc.getProyect().getLanguage());

    std::shared_ptr<corre::Colorize> col = lan->getColorize(doc);
    col->setFunctionSetColor(std::bind(&ColorizeText::setColor, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    setCurrentBlockState(col->colorizeLine(text.toStdString(), previousBlockState() == 1));
}

ColorizeText::ColorizeText(QTextDocument * parent , Document doc)
    : QSyntaxHighlighter(parent)
{
    this->doc = doc;
}
