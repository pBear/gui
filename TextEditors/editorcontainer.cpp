#include "editorcontainer.h"
#include "ui_editorcontainer.h"

#include "plugincontainer.h"

#include <QMenu>
#include <QDesktopServices>
#include <QProcess>

using namespace pBear::gui;

QMenu *menu;
QSettings settings("pBear", "Editor");

void EditorContainer::getPos(int &i, int &j, QGridLayout *layout, EditorWidget *editor)
{
    for (int x = 0; x < layout->rowCount(); x++)
    {
        for (int y = 0; y < layout->columnCount(); y++)
        {
            if (layout->itemAtPosition(x, y)->widget() == editor)
            {
                i = x;
                j = y;
                return;
            }
        }
    }
}

void EditorContainer::getPos(int &i, int &j, QGridLayout *layout, QGridLayout *layout1)
{
    for (int x = 0; x < layout->rowCount(); i++)
    {
        for (int y = 0; y < layout->columnCount(); y++)
        {
            if (layout->itemAtPosition(x, y)->layout() == layout1)
            {
                i = x;
                j = y;
                return;
            }
        }
    }
}

QGridLayout *EditorContainer::getParentLayout(QGridLayout *currLayout, QGridLayout *layout1)
{
    for (int i = 0; i < currLayout->rowCount(); i++)
    {
        for (int j = 0; j < currLayout->columnCount(); j++)
        {
            if (currLayout->itemAtPosition(i, j)->layout() == layout1)
            {
                return currLayout;
            }
            if (!dynamic_cast<EditorWidget*>(currLayout->itemAtPosition(i, j)->widget()))
            {
                QGridLayout *tmp = nullptr;
                tmp = getParentLayout(dynamic_cast<QGridLayout*>(currLayout->itemAtPosition(i, j)->layout()), layout1);
                if (tmp) return tmp;
            }
        }
    }
    return nullptr;
}

void EditorContainer::clearUnused(QSplitter *lay)
{
    for (int i = 0; i < lay->count(); i++)
    {
        QSplitter* tmp = dynamic_cast<QSplitter*>(lay->widget(i));
        if (tmp)
        {
            clearUnused(tmp);
        }
    }
    for (int i = 0; i < lay->count(); i++)
    {
        QSplitter* tmp = dynamic_cast<QSplitter*>(lay->widget(i));
        if (tmp)
        {
            if (tmp->count() == 0) delete tmp;
        }
    }
}

void EditorContainer::clearComprime(QSplitter *lay)
{
    for (int i = 0; i < lay->count(); i++)
    {
        QSplitter* tmp = dynamic_cast<QSplitter*>(lay->widget(i));
        if (tmp)
        {
            clearComprime(tmp);
            if (tmp->count() == 1)
            {
                QWidget *wi = tmp->widget(0);
                lay->insertWidget(i, wi);
                wi->setProperty("splitter", QVariant::fromValue(lay));
                delete tmp;
            }
        }
    }
}

std::pair<bool, EditorWidget *> EditorContainer::getNextCurrentEditor(QSplitter *lay, EditorWidget *editor)
{
    bool found = false;
    for (int i = 0; i < lay->count(); i++)
    {
        QSplitter* tmp = dynamic_cast<QSplitter*>(lay->widget(i));
        if (tmp)
        {
            std::pair<bool, EditorWidget *> pair = getNextCurrentEditor(tmp, editor);
            if (pair.first and pair.second) return pair;
            else if(pair.first and !pair.second) found = true;
        }
        if (lay->widget(i) == editor || found)
        {
            if (lay->count() == i - 1)
            {
                for (int j = i; j >= 0; j--)
                {
                    if (dynamic_cast<EditorWidget*>(lay->widget(i)))
                    {
                        return {true, dynamic_cast<EditorWidget*>(lay->widget(j))};
                    }
                }
            }
            else
            {
                for (int j = i; j < lay->count(); j++)
                {
                    if (dynamic_cast<EditorWidget*>(lay->widget(i)))
                    {
                        return {true, dynamic_cast<EditorWidget*>(lay->widget(j))};
                    }
                }
            }
            return {true, nullptr};
        }
    }
    return {false, nullptr};
}

QAction *openWithEditorIfAvaiable;

void EditorContainer::loadDoc(pBear::corre::Document doc)
{
    if (doc.getItem().empty())
    {
        QDesktopServices::openUrl(QUrl::fromLocalFile(doc.getName().c_str()));
    }
    else
    {
        if (!doc.getProyect().getItem(doc.getItem()).defaultEditor.empty())
        {
            if (openWithEditorIfAvaiable->isChecked())
            {
                QStringList list = {QString::fromStdString(doc.getName())};
                QProcess::startDetached(doc.getProyect().getItem(doc.getItem()).defaultEditor.c_str(), list);
            }
            else
            {
                QDesktopServices::openUrl(QUrl::fromLocalFile(doc.getName().c_str()));
            }
        }
        else
        {
            currentEditor->addDoc(doc);
        }
    }
}

EditorContainer::EditorContainer(std::shared_ptr<pBear::corre::Event> eve, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditorContainer)
{
    this->eve = eve;
    ui->setupUi(this);
    EditorWidget *editor   = new EditorWidget(eve, this);
    currentEditor = editor;
    editor->setFocusProxy(this);
    splitter = new QSplitter;
    splitter->adjustSize();
    ui->gridLayout->addWidget(splitter);
    editor->setProperty("splitter", QVariant::fromValue(splitter));
    connect(editor, SIGNAL(addHorizontal(EditorWidget*)), this, SLOT(addHorizontal(EditorWidget*)));
    connect(editor, SIGNAL(addVertical(EditorWidget*)), this, SLOT(addVertical(EditorWidget*)));
    connect(editor, SIGNAL(closeWid(EditorWidget*)), this, SLOT(closeEditor(EditorWidget*)));

    connect(editor, SIGNAL(focus(EditorWidget*)), this, SLOT(currentEditorChange(EditorWidget*)));

    splitter->addWidget(editor);


    //connect(QApplication::instance(), SIGNAL(focusChanged(QWidget *, QWidget * )), this, SLOT(focusChanged(QWidget*,QWidget*)));
    eve->addFuncDocLoad(std::bind(&EditorContainer::loadDoc, this, std::placeholders::_1));

    PluginContainer::instance()->init(eve, std::bind(&EditorContainer::slotRepaintPluginsUp, this));
}

void EditorContainer::repaintLocal()
{
    QWidget::repaint();
}

EditorContainer::~EditorContainer()
{
    delete ui;
}

void EditorContainer::slotRepaintPluginsUp()
{
    emit signalRepaintPluginsUp();
}

void EditorContainer::addHorizontal(EditorWidget *editor)
{
    EditorWidget *edit          = new EditorWidget(eve, this);
    QSplitter    *currentSplitter = editor->property("splitter").value<QSplitter*>();

    count++;

    if (currentSplitter->orientation() == Qt::Horizontal)
    {
        edit->setProperty("splitter", QVariant::fromValue(currentSplitter));
        currentSplitter->addWidget(edit);
    }
    else
    {
        QSplitter    *splitter      = new QSplitter(Qt::Horizontal);
        currentSplitter->insertWidget(currentSplitter->indexOf(editor), splitter);

        splitter->addWidget(editor);
        splitter->addWidget(edit);

        edit->setProperty("splitter", QVariant::fromValue(splitter));
        editor->setProperty("splitter", QVariant::fromValue(splitter));
    }

    edit->setFocusProxy(this);

    connect(edit, SIGNAL(focus(EditorWidget*)), this, SLOT(currentEditorChange(EditorWidget*)));

    connect(edit, SIGNAL(addHorizontal(EditorWidget*)), this, SLOT(addHorizontal(EditorWidget*)));
    connect(edit, SIGNAL(addVertical(EditorWidget*)), this, SLOT(addVertical(EditorWidget*)));
    connect(edit, SIGNAL(closeWid(EditorWidget*)), this, SLOT(closeEditor(EditorWidget*)));
}

void EditorContainer::addVertical(EditorWidget *editor)
{
    EditorWidget *edit          = new EditorWidget(eve, this);
    QSplitter    *currentSplitter = editor->property("splitter").value<QSplitter*>();

    count++;

    if (currentSplitter->orientation() == Qt::Vertical)
    {
        edit->setProperty("splitter", QVariant::fromValue(currentSplitter));
        currentSplitter->addWidget(edit);
    }
    else
    {
        QSplitter    *splitter      = new QSplitter(Qt::Vertical);
        currentSplitter->insertWidget(currentSplitter->indexOf(editor), splitter);

        splitter->addWidget(editor);
        splitter->addWidget(edit);

        edit->setProperty("splitter", QVariant::fromValue(splitter));
        editor->setProperty("splitter", QVariant::fromValue(splitter));
    }

    edit->setFocusProxy(this);

    connect(edit, SIGNAL(focus(EditorWidget*)), this, SLOT(currentEditorChange(EditorWidget*)));

    connect(edit, SIGNAL(addHorizontal(EditorWidget*)), this, SLOT(addHorizontal(EditorWidget*)));
    connect(edit, SIGNAL(addVertical(EditorWidget*)), this, SLOT(addVertical(EditorWidget*)));
    connect(edit, SIGNAL(closeWid(EditorWidget*)), this, SLOT(closeEditor(EditorWidget*)));
}

void EditorContainer::closeEditor(EditorWidget *editor)
{
    if (count > 1)
    {
        count--;
        std::pair<bool, EditorWidget *> pair = getNextCurrentEditor(splitter, editor);
        currentEditor = pair.second;
        std::cout << currentEditor << std::endl;
        delete editor;
    }
    clearUnused(splitter);
    clearComprime(splitter);
}

void EditorContainer::currentEditorChange(EditorWidget *editor)
{
    currentEditor = editor;
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new pBear::gui::EditorContainer(event);
    }
    std::string type()
    {
        return "CentralWidget";
    }
    std::string orientation()
    {
        return "central";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        menu = new QMenu("Editor");
        PluginContainer::instance()->load();
        menu->addSeparator();
        openWithEditorIfAvaiable = menu->addAction("Open with editor if possible");
        menu->addSeparator();
        openWithEditorIfAvaiable->setCheckable(true);
        if (settings.value("Open_with_editor_if_possible").toBool())
        {
            openWithEditorIfAvaiable->setChecked(true);
        }
        menu->addSeparator();
        QAction *save = menu->addAction("Save");
        save->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
        save->connect(save, &QAction::triggered, [event]()
        {
            for (pBear::corre::Project pro: *event->getOpenDocuments())
            {
                for (pBear::corre::Document doc: pro)
                {
                    doc.save();
                }
            }
        });
        return menu;
    }
    void close()
    {
        PluginContainer::instance()->close();
        settings.setValue("Open_with_editor_if_possible", openWithEditorIfAvaiable->isChecked());
    }
}
