#ifndef EDITORPLUGIN_H
#define EDITORPLUGIN_H

#include <block.h>
#include <document.h>


class EditorPlugin
{
public:
    std::string name;
    std::function<void(pBear::corre::Block*)>        mouseClickEvent;
    std::function<void(pBear::corre::Block*)>        mouseDoubleClickEvent;
    std::function<void(pBear::corre::Block*)>        mouseMoveEvent;
    std::function<std::string(pBear::corre::Block*)> icon;
    std::function<int()> weight;
    std::function<bool(pBear::corre::Document)>      hide;
    std::function<void(pBear::corre::Block*)>        defaultAction;

    friend bool operator<(const EditorPlugin &plugin1, const EditorPlugin &plugin2)
    {
        return plugin1.name < plugin2.name;
    }
    bool operator==(const EditorPlugin &plugin2)
    {
        return name == plugin2.name;
    }
};

#endif // EDITORPLUGIN_H
