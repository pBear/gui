#include "editorwidget.h"
#include "ui_editorwidget.h"
#include "project.h"
#include "event.h"
#include "editorcontainer.h"

#include <iostream>
#include <ctgmath>
#include <functional>

using namespace pBear::gui;

void EditorWidget::changeCurrentTab(int curr)
{
    if (curr < 0) return;

    if (last)
    {
        disconnect(last, SIGNAL(cursorPositionChanged()), this,
                   SLOT(cursorPositionChanged()));
    }

    Editor* last = static_cast<Editor*>(ui->TabEditors->widget(curr));
    connect(last->getTextEdit(),
            SIGNAL(cursorPositionChanged()), this,
            SLOT(cursorPositionChanged()));
    cursorPositionChanged();

    updateItemTypes();
    last->upLineData();
}

void EditorWidget::cursorPositionChanged()
{
    if (!getCurrentEditor()) return;

    QTextCursor cur = getCurrentEditor()->getTextEdit()->textCursor();
    std::string     str = "Ln: " + std::to_string(cur.blockNumber() + 1) +
                     " Col: " + std::to_string(cur.columnNumber() + 1) +
                     " Pos: " + std::to_string(cur.position());
    ui->Info->setText(str.c_str());
}

void EditorWidget::changeTab(int i)
{
    for (int cur = 0, n = ui->TabEditors->count(); cur < n; cur++)
    {
        int weight = getEditor(cur)->getTextEdit()->fontMetrics().width(' ');
        getEditor(cur)->getTextEdit()->setTabStopWidth(weight*(std::pow(2, i + 1) + 1));
    }
}

void EditorWidget::toolClickedSlot()
{
    emit toolClicked(getCurrentEditor()->getDoc());
}

void EditorWidget::tabClosedSlot(int i)
{
    emit tabClosed(getEditor(i)->getDoc());
    getEditor(i)->getDoc().setLoadFlag(false);
    ui->TabEditors->removeTab(i);
}

void EditorWidget::splitClick(int i)
{
    switch (i) {
    case 0:
        emit addHorizontal(this);
        break;
    case 1:
        emit addVertical(this);
    default:
        break;
    }
}

void EditorWidget::forwardClose()
{
    emit closeWid(this);
}

Editor *EditorWidget::getCurrentEditor()
{
    if (ui->TabEditors->currentIndex() < 0) return nullptr;
    return getEditor(ui->TabEditors->currentIndex());
}

Editor *EditorWidget::getEditor(int i)
{
    return static_cast<Editor*>(ui->TabEditors->widget(i));
}

Editor *EditorWidget::getEditor(pBear::corre::Document doc)
{
    int index = getIndex(doc);
    return index >= 0? getEditor(index): nullptr;
}

int EditorWidget::getIndex(corre::Document doc)
{
    for (int i = 0, n = ui->TabEditors->count(); i < n; i++)
    {
        if (getEditor(i)->getDoc() == doc)
        {
            return i;
        }
    }
    return -1;
}

void EditorWidget::updateItemTypes()
{
    block = true;

    ui->Language->clear();
    ui->Language->addItem("None");

    int cur = 0, i = 1;

    for (auto x: *corre::Language::instance())
    {
        for (corre::LanguageDatas::Item it: x.second->getItems())
        {
            ui->Language->addItem(it.name.c_str());
            if (ui->TabEditors->count() != 0)
            {
                if (getCurrentEditor()->getDoc().getItem() == it.name)
                {
                    cur = i;
                }
            }
            i++;
        }
    }
    ui->Language->setCurrentIndex(cur);
    block = false;
}

bool EditorWidget::event(QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress || event->type() == QEvent::KeyPress)
    {
        if (ui->TabEditors->currentWidget())
        {
            eve->getOpenDocuments()->setCurrentDoc(dynamic_cast<Editor*>(ui->TabEditors->currentWidget())->getDoc());
        }
        emit focus(this);
    }

    QDialog::event(event);
}

EditorWidget::EditorWidget(std::shared_ptr<corre::Event> eve, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditorWidget)
{
    connect(dynamic_cast<EditorContainer*>(parent), SIGNAL(signalRepaintPluginsUp()),
            this, SIGNAL(signalRepaintPlugins()));

    block = false;

    ui->setupUi(this);

    this->eve = eve;

    connect(ui->TabSpace, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTab(int)));
    connect(ui->TabEditors, SIGNAL(currentChanged(int)), this, SLOT(changeCurrentTab(int)));
    connect(ui->tool, SIGNAL(clicked()), this, SLOT(toolClickedSlot()));
    connect(ui->TabEditors, SIGNAL(tabCloseRequested(int)), this, SLOT(tabClosedSlot(int)));
    connect(ui->Language, SIGNAL(currentIndexChanged(QString)), this, SLOT(changeItemType(QString)));
    connect(ui->close, SIGNAL(clicked()), this, SLOT(forwardClose()));
    connect(ui->addHorVer, SIGNAL(activated(int)), this, SLOT(splitClick(int)));
    //connect(ui->addHorVer->view()->pressed)

    ui->TabEditors->setTabsClosable(true);

    updateItemTypes();

    setFocusPolicy(Qt::WheelFocus);

    eve->addFuncChangeLanguage(std::bind(&pBear::gui::EditorWidget::changeLan, this, std::placeholders::_1));
    eve->addFuncDocUnload(std::bind(&EditorWidget::removeDoc, this, std::placeholders::_1));
    eve->addFuncSaveDoc(std::bind(&EditorWidget::saveChange, this, std::placeholders::_1));
    eve->addFuncDocChangeCurrent(std::bind(&EditorWidget::changeCurrentDoc, this, std::placeholders::_1));
}

EditorWidget::~EditorWidget()
{
    delete ui;
}

bool EditorWidget::containDoc(pBear::corre::Document doc)
{
    for (int i = 0, n = ui->TabEditors->count(); i < n; i++)
    {
        if (getEditor(i)->getDoc() == doc)
        {
            return true;
        }
    }
    return false;
}

bool EditorWidget::addDoc(pBear::corre::Document doc)
{
    if (containDoc(doc)) return false;

    ui->TabEditors->addTab(new Editor(doc, this), doc.getRelativeNameToPro().c_str());

    changeTab(ui->TabSpace->currentIndex());

    changeCurrentDoc(doc);


    return true;
}

bool EditorWidget::removeDoc(pBear::corre::Document doc)
{
    for (int i = 0, n = ui->TabEditors->count(); i < n; i++)
    {
        if (getEditor(i)->getDoc() == doc)
        {
            ui->TabEditors->removeTab(i);
            return true;
        }
    }
    return false;
}

void EditorWidget::changeItemType(QString str)
{
    if (ui->TabEditors->count() <= 0) return;

    if (block)
    {
        return;
    }

    if (str == "None")
    {
        getCurrentEditor()->getDoc().setItem("");
    }
    else
    {
        getCurrentEditor()->
                getDoc().setItem(str.toStdString());
    }
}

void EditorWidget::changeLan(pBear::corre::Project pro)
{
    block = true;
    if (ui->TabEditors->count() != 0)
    {
        corre::Project pr = getCurrentEditor()->getDoc().getProyect();
        for (int i = 0, n = ui->TabEditors->count(); i < n; i++)
        {
            if (pr == getEditor(i)->getDoc().getProyect())
            {
                getEditor(i)->updateLanguage();
            }
        }
    }
    block = false;
}

void EditorWidget::saveChange(corre::Document doc)
{
    if (!containDoc(doc)) return;

    int index = getIndex(doc);
    std::string name = ui->TabEditors->tabText(index).toStdString();

    if (!doc.getSaved())
    {
        ui->TabEditors->setTabText(index, name[0] == '*'? QString::fromStdString(name): QString::fromStdString("*" + name));
    }
    else
    {
        ui->TabEditors->setTabText(index, name[0] == '*'? QString::fromStdString(name.substr(1)): QString::fromStdString(name));
    }
}

void EditorWidget::changeCurrentDoc(pBear::corre::Document doc)
{
    if (doc and containDoc(doc))
    {
        ui->TabEditors->setCurrentIndex(getIndex(doc));
    }
}

