#ifndef EDITORWIDGET_H
#define EDITORWIDGET_H

#include "document.h"
#include "opendocuments.h"

#include "editor.h"

#include <list>

#include <QDialog>
#include <QTextEdit>
#include <QTabWidget>
#include <QTextCursor>

namespace Ui {
class EditorWidget;
}
namespace pBear {
    namespace gui {
            class EditorWidget : public QDialog
            {
                Q_OBJECT
            private:
                Ui::EditorWidget *ui;
                std::shared_ptr<corre::Event> eve;
                Editor*           last = nullptr;
                std::atomic<bool> block;

                Editor* getCurrentEditor();
                Editor* getEditor(int i);
                Editor* getEditor(corre::Document doc);
                int     getIndex(corre::Document doc);
                void    updateItemTypes();
                bool    event(QEvent * event);
            public:
                explicit EditorWidget(std::shared_ptr<corre::Event> eve ,QWidget *parent = 0);
                ~EditorWidget();
                bool containDoc(corre::Document doc);
                bool addDoc(corre::Document doc);
                bool removeDoc(corre::Document doc);
                std::list<corre::Document> getDocs();
                void changeLan(corre::Project pro);
                void saveChange(corre::Document doc);
                void changeCurrentDoc(corre::Document doc);
            private slots:
                void changeItemType(QString str);
                void changeCurrentTab(int curr);
                void cursorPositionChanged();
                void changeTab(int i);
                void toolClickedSlot();
                void tabClosedSlot(int i);
                void splitClick(int i);
                void forwardClose();
            signals:
                void signalRepaintPlugins();
                void toolClicked(pBear::corre::Document&);
                void tabClosed(pBear::corre::Document&);
                void closeWid(EditorWidget *editor);
                void addHorizontal(EditorWidget *editor);
                void addVertical(EditorWidget *editor);
                void focus(EditorWidget* wid);
            };
        }
}

#endif // EDITORWIDGET_H
