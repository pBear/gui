#ifndef LINE_H
#define LINE_H

#include "document.h"

#include <QWidget>

namespace pBear {
    namespace gui {
        class TextEditWidget;
        class Line : public QWidget
        {
            Q_OBJECT
        private:
            corre::Document doc;

            std::vector<unsigned> lines;
            std::vector<unsigned> top;
            std::vector<unsigned> booton;
            int                   maxDigits;
            bool                  isVisibleErrorPresent = false;
            unsigned              nlines;
            unsigned              lLine        = 22222;
            unsigned              boldLine     = 0;
            unsigned              number_size  = 9;
            unsigned              error_size   = 15;
            int                   line_left;
            unsigned              error_left;
            unsigned              collapse_size = 6;
            bool                  once          = true;
            bool                  once1         = false;
            int                   width         = 0;
            TextEditWidget*       textEditWidget = nullptr;

            void     paintPlugins(unsigned top, unsigned bottom, corre::Document::iterator it);
            void     paintLine(unsigned top, unsigned botton, unsigned line);
            void     paintError(unsigned top, unsigned botton,
                                corre::Document::iterator it);
            void     paintCollapse(unsigned top, unsigned botton,
                                   corre::Document::iterator it);
            corre::Document::iterator getBlockByPos(unsigned pos);
            void mousePressEvent(QMouseEvent*);
            void mouseDoubleClickEvent(QMouseEvent*);
            void mouseMoveEvent(QMouseEvent*);
            void updateWight();
        public:
            explicit Line(QWidget *parent = 0);
            ~Line();
            void     paintEvent(QPaintEvent *);
            void     repaint(std::vector<unsigned>    lines,
                                std::vector<unsigned> top,
                                std::vector<unsigned> booton);
            void repaint();
            void setDoc(const corre::Document &value);
            void setTextEditWidget(TextEditWidget* wid);
        public slots:
            void repaintPlugins();

        };
    }
}

#endif // LINE_H
