#include <analizerbreakpointsgdb.h>

#include "Controller.hpp"




#include <event.h>

#include <iostream>

Controller* Controller::once = nullptr;

Controller* Controller::instance()
{
    if (!once) once = new Controller;
    return once;
}

void Controller::init(std::shared_ptr<pBear::corre::Event> event)
{
    this->event = event;
}

void Controller::start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (this->deb == nullptr)
    {
        this->deb = deb;
        deb->getControler()->getAnalizer<pBear::debuger::AnalizerBreakpointsGDB>()->addUpdateFunction(std::bind(&Controller::updateBreapoins, this, std::placeholders::_1));
        for (pBear::corre::Project pro: *event->getOpenDocuments())
        {
            for (pBear::corre::Document doc: pro)
            {
                for (pBear::corre::Block &block: doc)
                {
                    if (block.getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints"))
                    {
                        for (pBear::debuger::Breakpoint breakpoint: *block.getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints"))
                        {
                            deb->getControler()->setBreakpoint(breakpoint.file, breakpoint.line);
                        }
                    }
                }
            }
        }
    }
}

void Controller::end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (deb == this->deb) this->deb = nullptr;
}

void Controller::updateBreapoins(std::list<pBear::debuger::Breakpoint> breaks)
{
    event->getOpenDocuments()->clearBlockData<std::list<pBear::debuger::Breakpoint>>("Breakpoints");
    for (pBear::debuger::Breakpoint x: breaks)
    {
        pBear::corre::Document doc = event->getOpenDocuments()->getDocByName(x.fullname);
        if (!doc)
        {
            if (event->getOpenDocuments()->getCurrentPro())
            {
                doc = event->getOpenDocuments()->getCurrentPro().newDocument(x.fullname);
            }
        }
        if (doc)
        {
            pBear::corre::Block *block = &(*doc.findByLine(x.line));
            if (!block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints"))
            {
                block->setPluginData("Breakpoints", (void*)(new std::list<pBear::debuger::Breakpoint>));
            }
            block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->push_back(x);
        }
    }
    executeRepaint();
}

void Controller::setRepaintFunction(std::function<void ()> funRepaint)
{
    this->funRepaint = funRepaint;
}

void Controller::executeRepaint()
{
    funRepaint();
}

void Controller::clickEvent(pBear::corre::Block *block)
{
    if (!block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints"))
    {
        block->setPluginData("Breakpoints", (void*)(new std::list<pBear::debuger::Breakpoint>));
    }

    if (block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->empty())
    {
        pBear::debuger::Breakpoint tmp;
        tmp.line = block->getLine();
        tmp.file = block->getDocument().getName();
        tmp.disposition = true;
        tmp.enable      = true;
        tmp.ignore      = false;
        tmp.number      = -1;

        block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->push_back(tmp);

        if (deb)
        {
            deb->getControler()->setBreakpoint(block->getDocument().getName(), block->getLine());
        }
    }
    else
    {
        if (deb)
        {
            for (pBear::debuger::Breakpoint br: *block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints"))
            {
                deb->getControler()->deleteBreakpoint(br.number);
            }
        }
        block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->clear();
    }

    funRepaint();
}

void Controller::defaultAction(pBear::corre::Block *block)
{
    clickEvent(block);
}

Controller::Controller()
{
    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(std::bind(&Controller::start, this, std::placeholders::_1, std::placeholders::_2));
    pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(std::bind(&Controller::end, this, std::placeholders::_1, std::placeholders::_2));
}

extern "C" {
    void init(std::shared_ptr<pBear::corre::Event> event)
    {
        Controller::instance()->init(event);
        Controller::instance()->executeRepaint();
    }

    void mouseClickEvent(pBear::corre::Block* block)
    {
        Controller::instance()->clickEvent(block);
    }

    void mouseDoubleClickEvent(pBear::corre::Block* block)
    {
        std::cout << "double click: " << block->getLine() << std::endl;
    }

    void mouseMoveEvent(pBear::corre::Block* block)
    {
        std::cout << "move: " << block->getLine() << std::endl;
    }

    std::string icon(pBear::corre::Block* block)
    {
        if (!block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")) return "";
        if (block->getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->empty()) return "";
        return "/home/aron/pBear/gui/TextEditors/pluginDebuger/Breakpoint.png";
    }

    int weight()
    {
        return 14;
    }

    bool hide(pBear::corre::Document doc)
    {
        if  (!doc) return true;
        bool state = true;
        for (pBear::corre::Block &block: doc)
        {
            if (!block.getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")) continue;
            if (block.getPluginData<std::list<pBear::debuger::Breakpoint>>("Breakpoints")->empty()) continue;
            state = false;
        }
        return state;
    }

    void setRepaintFunction(std::function<void()> funRepaint)
    {
        Controller::instance()->setRepaintFunction(funRepaint);
    }
    void defaultAction(pBear::corre::Block *block)
    {
        Controller::instance()->defaultAction(block);
    }
}

