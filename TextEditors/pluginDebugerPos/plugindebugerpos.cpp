#include "plugindebugerpos.h"

#include <analizerstopgdb.h>

#include <analizeframestackgdb.h>

pBear::debuger::Frame fr;

void action(pBear::debuger::StopReason stopReason)
{
    fr = stopReason.frame;
    funRepaint();
}

void actionCurrFrame(pBear::debuger::Frame curr)
{
    std::cout << curr.level << " " << fr.level << std::endl;
    if (fr.line == -1 || curr.level != fr.level)
    {
        fr = curr;
    }
    funRepaint();
}

void start(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (::pro != pro)
    {
        ::deb = deb;
        ::pro = pro;
        deb->getControler()->getAnalizer<pBear::debuger::AnalizerStopGDB>()->addEventFunction(action);
        deb->getControler()->getAnalizer<pBear::debuger::AnalizeFrameStack>()->addFunctionCurr(actionCurrFrame);
    }
}

void end(std::shared_ptr<pBear::ConnectionClient::Process> pro, pBear::debuger::TerminalProcessConnectorDebuger *deb)
{
    if (::pro == pro)
    {
        pro.reset();
        ::deb = nullptr;
        fr.line = -1;
        fr.fullname = "";
        funRepaint();
    }
}

extern "C" {
    void init(std::shared_ptr<pBear::corre::Event> event)
    {
        ::event = event;
        pBear::debuger::TerminalProcessConnectorDebuger::addFunctionStart(start);
        pBear::debuger::TerminalProcessConnectorDebuger::addFunctionEnd(end);
    }

    void mouseClickEvent(pBear::corre::Block* block)
    {

    }

    void mouseDoubleClickEvent(pBear::corre::Block* block)
    {

    }

    void mouseMoveEvent(pBear::corre::Block* block)
    {

    }

    std::string icon(pBear::corre::Block* block)
    {
        if (block->getLine() == fr.line && block->getDocument().getName() == fr.fullname)
        {
            return "/home/aron/pBear/gui/TextEditors/pluginDebugerPos/pos.png";
        }
        else
        {
            return "";
        }
    }

    int weight()
    {
        return 19;
    }

    bool hide(pBear::corre::Document doc)
    {
        if  (!doc) return true;
        if (doc.getName() == fr.fullname)
        {
            return false;
        }
        return true;
    }

    void setRepaintFunction(std::function<void()> funRepaint)
    {
        ::funRepaint = funRepaint;
    }
    void defaultAction(pBear::corre::Block *block)
    {

    }
}



