#include "textedit.h"

#include "analizer.h"
using namespace pBear::gui;
using namespace pBear::corre;


AutocompleteMenu1::AutocompleteMenu1(Document doc, QTextEdit *edit, QWidget *parent):
    QListWidget(parent)
{
    doc = doc;
    edit = edit;
}

void AutocompleteMenu1::exec()
{
    QTextCursor cur = edit->textCursor();
    doc.getAnalizer()->getCompUseful(cur.blockNumber(), cur.columnNumber(), cur.position());
    move(mapToGlobal(edit->cursorRect(edit->textCursor()).bottomLeft()));
    show();
}
