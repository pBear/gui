#include "texteditwidget.h"
#include "editorcontainer.h"

#include <iostream>

using namespace pBear::gui;

void TextEditWidget::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Space:
        if (event->modifiers()==Qt::ControlModifier)
        {
            autocomplet->exec();
        }
        else
        {
            QPlainTextEdit::keyPressEvent(event);
        }
        break;
    default:
        QPlainTextEdit::keyPressEvent(event);
    }
}

void TextEditWidget::keyReleaseEvent(QKeyEvent *event)
{
    QPlainTextEdit::keyReleaseEvent(event);
}

void TextEditWidget::focusInEvent(QFocusEvent *e)
{
    if (doc)
    {
        doc.getProyect().getOpenDocuments()->setCurrentDoc(doc);
    }
    QPlainTextEdit::focusInEvent(e);
}

TextEditWidget::TextEditWidget(QWidget *parent):
    QPlainTextEdit(parent)
{


    autocomplet = std::make_shared<AutocompleteMenu>(this, this);
    autocomplet->hide();

    connect(autocomplet.get(), SIGNAL(keyPress(QKeyEvent*)), this, SLOT(keyPressEvent(QKeyEvent*)));
    connect(autocomplet.get(), SIGNAL(keyRell(QKeyEvent*)), this, SLOT(keyReleaseEvent(QKeyEvent*)));

    setLineWrapMode(NoWrap);
}

void TextEditWidget::setDocument(pBear::corre::Document doc)
{
    this->doc = doc;
    autocomplet->setDoc(doc);

    doc.getProyect().getOpenDocuments()->getEvent()->addFuncDocUpdate(std::bind(&TextEditWidget::updateDocEvent, this,
                                                                                std::placeholders::_1, std::placeholders::_2,
                                                                                std::placeholders::_3, std::placeholders::_4,
                                                                                std::placeholders::_5, std::placeholders::_6));
}

void TextEditWidget::updateDocEvent(pBear::corre::Document doc, corre::Document::iterator it, int pos, int remove, int add,std::string text)
{
    if (this->doc == doc)
    {
        if (toPlainText() != QString::fromStdString(doc.getText()))
        {
            QTextCursor cur = textCursor();
            cur.movePosition(QTextCursor::Start);
            cur.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, pos);
            cur.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, remove);
            cur.insertText(text.c_str());
        }
    }
}
