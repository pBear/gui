#ifndef TEXTEDITWIDGET_H
#define TEXTEDITWIDGET_H

#include "autocompletmenu.h"
#include <QPlainTextEdit>

namespace pBear
{
    namespace gui
    {
        class TextEditWidget: public QPlainTextEdit
        {
            Q_OBJECT
        private:
             std::shared_ptr<AutocompleteMenu> autocomplet;
             corre::Document                   doc;

             void focusInEvent(QFocusEvent *e);
        public:
            explicit TextEditWidget(QWidget *parent);
            void setDocument(corre::Document doc);
            //event
            void             updateDocEvent(corre::Document doc, corre::Document::iterator it, int pos, int remove, int add, std::string text);
        private slots:
            void keyPressEvent(QKeyEvent *event);
            void keyReleaseEvent(QKeyEvent *event);
        signals:
            void signalRepaintPlugins();
        };
    }
}

#endif // TEXTEDITWIDGET_H
