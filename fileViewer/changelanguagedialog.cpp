#include "changelanguagedialog.h"
#include "ui_changelanguagedialog.h"
#include "language.h"
#include "event.h"

#include <iostream>
itemChangeLanguageDialogClose::itemChangeLanguageDialogClose(QWidget *parent)
{
    buttonMoveToTrush.setText("Move to trush");
    buttonClose.setText("close");

    setLayout(&layout);
    layout.addWidget(&buttonMoveToTrush);
    layout.addWidget(&buttonClose);
    layout.setMargin(0);

    connect(&buttonClose, SIGNAL(clicked()), this, SIGNAL(close()));
    connect(&buttonMoveToTrush, SIGNAL(clicked()), this, SIGNAL(moveToTrush()));

    show();
}

itemChangeLanguageDialog::itemChangeLanguageDialog(pBear::corre::Document doc):
    doc(doc)
{
    layout = std::make_shared<QHBoxLayout>(this);
    file   = std::make_shared<QLabel>();
    comb   = std::make_shared<QComboBox>();
    note   = std::make_shared<QLabel>();
    close  = std::make_shared<QToolButton>();;

    close->setStyleSheet("border: 0px");
    close->setText("X");


    changeLanguage();

    note->setStyleSheet("color: red");

    layout->addWidget(file.get());
    layout->addWidget(comb.get());
    layout->addWidget(note.get());
    layout->addWidget(close.get());


    layout->setMargin(0);

    file->setText(QString::fromStdString(doc.getName()));

    connect(close.get(),SIGNAL(clicked()),this,SLOT(end()));
    connect(comb.get(), SIGNAL(currentIndexChanged(int)), this, SLOT(chageItemType(int)));
}

QString  itemChangeLanguageDialog::getName()
{
    return file->text();
}

void itemChangeLanguageDialog::changeLanguage()
{
    std::string im;
    comb->clear();
    if (doc.getProyect().getLanguage() == "")
    {
        comb->addItem("general");
    }
    else
    {
        comb->addItem("none");
        im = doc.getProyect().getFileType(doc.getName());
        int index = 0;
        for (pBear::corre::LanguageDatas::Item item: doc.getProyect().getItems())
        {
            comb->addItem(item.name.c_str());
            if (im == item.name)
            {
                comb->setCurrentIndex(index);
            }
            index++;
        }
    }
}

void itemChangeLanguageDialog::end()
{
    dia = new itemChangeLanguageDialogClose(this);
    connect(dia, SIGNAL(close()), this, SLOT(closeEvent()));
    connect(dia, SIGNAL(moveToTrush()), this, SLOT(moveToTrushEvent()));
}

void itemChangeLanguageDialog::closeEvent()
{
    doc.close();
    disconnect(dia, SIGNAL(close()), this, SLOT(closeEvent()));
    disconnect(dia, SIGNAL(moveToTrush()), this, SLOT(moveToTrushEvent()));
    delete dia;
}

void itemChangeLanguageDialog::moveToTrushEvent()
{
    doc.moveToTrush();
    disconnect(dia, SIGNAL(close()), this, SLOT(closeEvent()));
    disconnect(dia, SIGNAL(moveToTrush()), this, SLOT(moveToTrushEvent()));
    dia->hide();
    delete dia;
}

void itemChangeLanguageDialog::chageItemType(int)
{
    doc.setItem(comb->currentText().toStdString());
}

void ChangeLanguageDialog::setComboBoxData(QString item)
{
    int it = 0, count = 0;
    for (std::pair<std::string, pBear::corre::LanguageDatas*> x: *pBear::corre::Language::instance())
    {
        ui->comboBox->addItem(QString::fromStdString(x.first));
        if (item.toStdString() == x.first)
        {
            it = count;
        }
        count++;
    }
    ui->comboBox->setCurrentIndex(it);
}


ChangeLanguageDialog::ChangeLanguageDialog(QWidget *parent, pBear::corre::Project pro, QString item) :
    QDialog(parent), ui(new Ui::ChangeLanguageDialog), pro(pro)
{
    ui->setupUi(this);
    setComboBoxData(item);
    setItems();
    show();
}

void ChangeLanguageDialog::setItems()
{
    for (pBear::corre::Document doc: pro)
    {
        if (doc.hasEquivalentId())
        {
            QListWidgetItem          *item = new QListWidgetItem;
            itemChangeLanguageDialog *wid  = new itemChangeLanguageDialog(doc);
            ui->listWidget->addItem(item);
            ui->listWidget->setItemWidget(item, wid);
        }
    }
}

ChangeLanguageDialog::~ChangeLanguageDialog()
{
    delete ui;
}

void ChangeLanguageDialog::accept()
{
    pro.setLanguage(ui->comboBox->currentText().toStdString());
    QDialog::accept();
}
