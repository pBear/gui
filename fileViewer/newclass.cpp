#include "newclass.h"
#include "ui_dialog.h"
#include "event.h"

#include <QFileInfo>
#include <iostream>
#include <QLineEdit>
#include <QFileDialog>

newClass::newClass(corre::Event *event, QWidget *parent) :
    QDialog(parent),
    event(event),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    ui->lineEdit_2->setText(".hpp");
    ui->lineEdit->setText(".cpp");
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(source_changed(QString)));
    connect(ui->lineEdit_2,SIGNAL(textChanged(QString)),this,SLOT(head_changed(QString)));
    ui->lineEdit_2->setCursorPosition(0);
}

newClass::~newClass()
{
    delete ui;
}

void newClass::head_changed(QString name)
{
    QFileInfo ext(name);
    QFileInfo extant(ui->lineEdit->text());
    if(head_bef!=extant.baseName())
        return;
    ui->lineEdit->disconnect(this);
    ui->lineEdit->setText(ext.baseName()+".cpp");
    head_bef=ext.baseName();
}

void newClass::source_changed(QString name)
{
    QFileInfo ext(name);
    QFileInfo extant(ui->lineEdit_2->text());
    if(source_bef!=extant.baseName())
        return;
    ui->lineEdit_2->disconnect(this);
    ui->lineEdit_2->setText(ext.baseName()+".hpp");
    source_bef=ext.baseName();
}

void newClass::accept()
{
    corre::Document doc("");

    pro.setLanguage(ui->language->currentText().toStdString());


    //add header
    doc.setName(ui->lineEdit_3->text().toStdString());
    doc.setItem("header");
    doc.setProyect(pro);
    event->executeFuncNewDoc(doc);

    //add source
    doc.setName(ui->lineEdit_4->text().toStdString());
    doc.setItem("source");
    event->executeFuncNewDoc(doc);

    ui->lineEdit->disconnect(this);
    ui->lineEdit_2->disconnect(this);
    head_bef="";
    source_bef="";
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(source_changed(QString)));
    connect(ui->lineEdit_2,SIGNAL(textChanged(QString)),this,SLOT(head_changed(QString)));
    close();
}

void newClass::sourceHeadFind()
{
    QString name=QFileDialog::getExistingDirectory(this,"Directorio en el que se creara la clase",".");
    ui->lineEdit_3->setText(name+"/"+ui->lineEdit_2->text());
    ui->lineEdit_4->setText(name+"/"+ui->lineEdit->text());
}

void newClass::headFind()
{
    QString name=QFileDialog::getExistingDirectory(this,"Directorio en el que se creara la cabcera",".");
    ui->lineEdit_3->setText(name+"/"+ui->lineEdit_2->text());
}

void newClass::show(corre::Project pro)
{
    this->pro = pro;
    if (pro.getLanguage() != "")
    {
        ui->language->hide();
    }
    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

void newClass::sourceFind()
{
    QString name=QFileDialog::getExistingDirectory(this,"Directorio en el que se creara la fuente",".");
    ui->lineEdit_4->setText(name+"/"+ui->lineEdit->text());
}
