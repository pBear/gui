#include "newfile.h"
#include "ui_newfile.h"
#include "event.h"
#include "language.h"
#include "opendocuments.h"

#include <QPushButton>


newFile *newFile::once = nullptr;

newFile::newFile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::newFile)
{
    ui->setupUi(this);
    //ui->verticalLayout->insertWidget(0, &chosePro);
    connect(ui->toolButton,SIGNAL(clicked()),this,SLOT(fileFind()));
    connect(ui->lineEdit,SIGNAL(textChanged(QString)),this,SLOT(setChanged(QString)));
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged(int)));
}

newFile *newFile::instance()
{
    if (!once) once = new newFile;
    return once;
}

void newFile::show(pBear::corre::Event* event, QString fType, QString sufix, QString _path, std::string itemName, pBear::corre::Project _pro, QWidget *widget)
{
    std::cout << itemName << std::endl;
    this->_pro  = _pro;
    this->event = event;
    this->setParent(widget);
    if (_pro.size())
    {
        ui->comboBox->hide();
    }
    else
    {
        ui->comboBox->show();
        ui->comboBox->clear();
        ui->comboBox->addItem("none");
        for (auto x: *pBear::corre::Language::instance())
        {
            ui->comboBox->addItem(x.first.c_str());
        }
    }
    this->itemName = itemName;
    if (sufix[0] != '.')
        ui->lineEdit->setText("." + sufix);
    else
        ui->lineEdit->setText(sufix);
    ui->lineEdit->setCursorPosition(0);
    if(_path != "") {
        path = _path;
        ui->lineEdit_2->setText(path+"/");
    }
    _fileType = fType;
    setWindowTitle("make a " + fType);
    setModal(true);
    setWindowFlags(Qt::Dialog);
    QDialog::show();
}

void newFile::accept() {
    pBear::corre::Document doc("");
    doc.setName(ui->lineEdit_2->text().toStdString());
    doc.setItem(itemName);
    doc.setProyect(_pro);
    std::cout << itemName << std::endl;
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    QDialog::accept();
}

void newFile::fileFind() {
    path = QFileDialog::getExistingDirectory(this,"Directorio en el que se creara la " + _fileType,".");
    ui->lineEdit_2->setText(path + "/" + ui->lineEdit->text());
}

void newFile::setChanged(QString) {
    if(ui->lineEdit_2->text() == "")
        return;
    ui->lineEdit_2->setText(path + "/" + ui->lineEdit->text());
}


newFile::~newFile()
{
    delete ui;
}

void newFile::currentIndexChanged(int index)
{
    switch (index)
    {
    case 0:
        ui->buttonBox->button(QDialogButtonBox::Ok)->hide();
        _pro.setLanguage("");
        return;
    default:
        _pro.setLanguage(ui->comboBox->itemText(index).toStdString());
        ui->buttonBox->button(QDialogButtonBox::Ok)->show();
    }
}
