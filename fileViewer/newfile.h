#ifndef NEWFILE_H
#define NEWFILE_H

#include "document.h"
#include "choseprotype.h"

#include <QDialog>
#include <QString>
#include <QFileDialog>

namespace Ui {
class newFile;
}

class newFile : public QDialog
{
    Q_OBJECT
private:
    QString         _fileType;
    QString               path;
    pBear::corre::Project  _pro;
    std::string     itemName;
    pBear::corre::Event *event;
    static newFile *once;
public:
    explicit newFile(QWidget *parent = 0);
    static newFile* instance();
    void     setPrefix(QString);
    ~newFile();

private:
    Ui::newFile *ui;
    pBear::choseProType chosePro;

private slots:
    void accept();
    void fileFind();
public slots:
    void show(pBear::corre::Event *event, QString, QString, QString, std::string itemName, pBear::corre::Project _pro, QWidget *widget);
    void setChanged(QString);
    void currentIndexChanged(int index);
};

#endif // NEWFILE_H
