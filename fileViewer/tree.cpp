#include "tree.h"
#include "openfiles.h"
#include "newfile.h"

#include <iostream>
#include <QEvent>
#include <QMouseEvent>
#include <QMenu>
#include <QTextDocument>
#include <event.h>
#include <QAction>

using namespace pBear;

void tree::costumizeMenuGroup(QString name, corre::Project pro)
{
    if (name == "additional") name = "";

    menuGroup.clear();
    if (pro.getItem(name.toStdString()).showAddMenu)
    {
        menuGroup.addAction("add");
    }
    if (pro.getItem(name.toStdString()).showOpenMenu)
    {
        menuGroup.addAction("open");
    }
}

tree::tree(corre::Event *event, QWidget *parent) :
    baseProyectView(0, event, parent),
    menuGroup(this)
{

    //the group menu
    menuGroup.addAction("add");
    menuGroup.addAction("open");

    //connect to slot
    connect(&menuGroup, SIGNAL(triggered(QAction*)), this, SLOT(doMenuGroup(QAction*)));
}

//add items
corre::Event::docData tree::addItem(corre::Document doc)
{   
    //error param managment
    _pbExeption.addFunction("tree::addItem");

    if (doc.getName().empty())
    {
        _pbExeption << "addItem";
        _pbExeption << "tree";
        _pbExeption << "the paremaeter doc has a empty name";
        throw _pbExeption;
    }
    /*if (doc.getItem() == "")
    {
        _pbExeption << "addItem";
        _pbExeption << "tree";
        _pbExeption << "the parameter doc has a type = NONE";
        throw _pbExeption;
    }*/

    //create item
    QTreeWidgetItem      *item = new QTreeWidgetItem;
    QString               name;
    corre::Event::docData data;


    //get relative doc name
    name = doc.getRelativeNameToPro().c_str();

    //set data
    item->setText(0, name);
    item->setFlags(Qt::ItemIsEditable| Qt::ItemIsSelectable| Qt::ItemIsEnabled);


    static_cast<QTreeWidgetItem*>(doc.getProyect().getItemToReal(doc.getItem(), id))->addChild(item);
    //set The icons and expand the item if it is necesary

    if (!doc.getItem().empty())
    {
        item->setIcon(0, QIcon(doc.getProyect().getItem(doc.getItem()).iconDoc.c_str()));
    }

    //set the item the courent
    //setCurrentItem(item);

    //return the doc item
    data.data = item;
    data.id   = id;
    return data;
}

void tree::addItem(corre::Project pro)
{
    //error managment
    _pbExeption.addFunction("tree::addItem");

    //variable declarations
    QTreeWidgetItem *item    = new QTreeWidgetItem;

    //set data
    if (!pro.getName().empty())
    {
        item->setText(0, pro.getName().c_str());
    }
    else
    {
        item->setText(0, "default");
    }
    pro.addEquivalentID(id, item);


    item->setFlags(Qt::ItemIsEditable| Qt::ItemIsSelectable| Qt::ItemIsEnabled);
    //add proyect
    addTopLevelItem(item);
    item->setExpanded(true);

    changeLangnuage(pro);

    for (corre::Document d: pro)
    {
        addItem(d);
    }
}

void tree::updateProyect(corre::Project pro)
{
    //error managment
    _pbExeption.addFunction("tree::updateProyect");
    if (pro.getEquivalentIDs().find(id) == pro.getEquivalentIDs().end())
    {
        _pbExeption << "updateProyect";
        _pbExeption << "tree";
        _pbExeption << "ther are not interface id for this id and proyect";
        throw _pbExeption;
    }

    //declarations
    QString name;

    //update the name
    if (pro.getName().empty())
    {
        name = "default";
    }
    else
    {
        name = pro.getName().c_str();
    }
    static_cast<QTreeWidgetItem*>(pro.getEquivalentID(id))->setText(0, name);

    //rename documents so that the name is in relation to the proyect name
    noUpdateDocItem = true;
    for (corre::Document doc: pro)
    {
        name = doc.getRelativeNameToPro().c_str();
        static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id))->setText(0, name);
    }
    noUpdateDocItem = false;
}

void tree::updateDocument(corre::Document doc)
{
    //error managment
    _pbExeption.addFunction("tree::updateDocument");
    if (doc.getEquivalentIDs().find(id) == doc.getEquivalentIDs().end())
    {
        return;
    }
    if (doc.getName().empty())
    {
        _pbExeption << "updateDocument";
        _pbExeption << "tree";
        _pbExeption << "the doc name es empty";
        throw _pbExeption;
    }
    //update the name
    //QTreeWidgetItem *parent = static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id));
    //parent->setText(0, doc.getName().c_str());
    //QTreeWidgetItem *child = parent->

    QTreeWidgetItem      *item = static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id));
    QString               name;
    corre::Event::docData data;

    item = item->parent()->takeChild(item->parent()->indexOfChild(item));

    //get relative doc name
    name = doc.getRelativeNameToPro().c_str();

    //set data
    item->setText(0, name);
    item->setFlags(Qt::ItemIsEditable| Qt::ItemIsSelectable| Qt::ItemIsEnabled);


    static_cast<QTreeWidgetItem*>(doc.getProyect().getItemToReal(doc.getItem(), id))->addChild(item);
    //set The icons and expand the item if it is necesary

    if (!doc.getItem().empty())
    {
        item->setIcon(0, QIcon(doc.getProyect().getItem(doc.getItem()).iconDoc.c_str()));
    }

}

void tree::closeDoc(corre::Document doc)
{
    delete static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id));
}

void tree::changeLangnuage(corre::Project pro)
{
    QTreeWidgetItem *subItem = NULL;


    QList<QTreeWidgetItem*> list = static_cast<QTreeWidgetItem*>(pro.getEquivalentID(id))->takeChildren();

    for (corre::LanguageDatas::Item x: pro.getItems())
    {
        if (x.show)
        {
            subItem = new QTreeWidgetItem;
            subItem->setIcon(0, QIcon(x.icon.c_str()));
            if (!x.name.empty())
            {
                subItem->setText(0, x.name.c_str());
            }
            else
            {
                subItem->setText(0, "additional");
            }
            pro.addItem(x.name, subItem, id);
            static_cast<QTreeWidgetItem*>(pro.getEquivalentID(id))->addChild(subItem);
        }
    }
    if (!list.isEmpty())
    {
        for (QTreeWidgetItem* it: list)
        {
            it->takeChildren();
        }
        for (corre::Document doc: pro)
        {
            if (doc.getEquivalentID(id))
            {
                QTreeWidgetItem *it = static_cast<QTreeWidgetItem*>(pro.getItemToReal(doc.getItem(), id));
                it->addChild(static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id)));
                if (!doc.getItem().empty())
                {
                    static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id))->setIcon(0, QIcon(doc.getProyect().getItem(doc.getItem()).iconDoc.c_str()));
                }
            }
        }
    }

    for (QTreeWidgetItem* it: list)
    {
        delete it;
    }
}

//doMenuAction

void tree::doMenuGroup(QAction *option)
{
    //the error managment
    _pbExeption.addFunction("tree::doMenuGroup");
    if (!option)
    {
        _pbExeption << "doMenuGroup";
        _pbExeption << "tree";
        _pbExeption << "the parameter QAction *option = " << option;
        throw _pbExeption;
    }

    //basic data
    QString                    name     = event->getOpenDocuments()->getProByGroup(itemClicked).getName().c_str();
    corre::Project             pPro     = event->getOpenDocuments()->getProByGroup(itemClicked);
    corre::LanguageDatas::Item itemData = pPro.getItem(pPro.getRealToItem(itemClicked));

    //actions
    if (option->text() == "add")
    {
        newFile::instance()->show(event, std::string(itemData.name + " file").c_str(), itemData.defaultExtencion.c_str(), name, itemData.name.c_str(), pPro, this);
    }
    if (option->text() == "open")
    {
         openFiles::instance()->show(event, pPro, true, itemData.name.c_str(), this);
    }
}



void  tree::itemClick(QTreeWidgetItem* item, int)
{
    _pbExeption.addFunction("tree::itemClick");
    //see if the parameter is valid
    if (!item)
    {
        _pbExeption << "itemClick";
        _pbExeption << "tree";
        _pbExeption << "the parameter QTreeWidgetItem* item is " << item;
        throw _pbExeption;
    }

    itemClicked = item;

    //show the corresponding menu
    if (menuActive)
    {
        menuActive  = false;
        switch (event->getOpenDocuments()->type(item))
        {
        case corre::itemType::PROYECT:
            prepareMenusForProject(event->getOpenDocuments()->traductPro(item));
            menuProy.exec(QCursor::pos());
            break;
        case corre::itemType::DOCUMENT:
            prepareMenusForFile(event->getOpenDocuments()->traductDoc(item));
            prepareMenusForProject(event->getOpenDocuments()->traductDoc(item).getProyect());
            menuFile.exec(QCursor::pos());
            break;
        case corre::itemType::GROUP:
            costumizeMenuGroup(item->text(0), event->getOpenDocuments()->getProByGroup(item));
            prepareMenusForProject(event->getOpenDocuments()->getProByGroup(item));
            menuGroup.exec(QCursor::pos());
            break;
        default:
            _pbExeption << "itemClick";
            _pbExeption << "tree";
            _pbExeption << "the item is item type is not known";
            throw _pbExeption;
        }
    }
}

void tree::loadDoc(corre::Document doc)
{
     static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id))->setBackgroundColor(0, QColor(qRgb(215, 198, 198)));
}

void  tree::unloadDoc(corre::Document doc)
{
    static_cast<QTreeWidgetItem*>(doc.getEquivalentID(id))->setBackgroundColor(0, QColor(qRgba(255, 255, 255,255)));
}

corre::Document tree::createDocByItem(DOC doc)
{
    return event->getOpenDocuments()->traductDoc(doc);
}

extern "C" {
    QWidget *getWidget(std::shared_ptr<pBear::corre::Event> event)
    {
        return new pBear::tree(event.get());
    }
    std::string type()
    {
        return "QDockWidget";
    }
    std::string orientation()
    {
        return "left";
    }
    QMenu* getMenu(std::shared_ptr<pBear::corre::Event> event)
    {
        return new QMenu("File Viewer");
    }
}
