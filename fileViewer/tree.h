/**@file tree.h
 * @author Aron Surian Wolf
 * @bug hereded buck: implement execute, nuild
 * @brief used to represent in a tree structure the headers, source, ... files
 * @date 10.5.2014
 *
 * The top level items are the proyects. Every proyect has got different containers
 * like "herers", "sources".... are exeptuation are the config files (CMAKE...) this are
 * on the same level as this containers.
 *
 * It is disigned in that wey that every change is make or is connected to the event manager.
 * so that changes can be reflected in other widgets
**/

#ifndef TREE_H
#define TREE_H

#include "baseproyectview.h"

///general namespace of the proyect
namespace pBear
{

class tree: public baseProyectView
{
    Q_OBJECT
private:
    //munus
    QMenu                     menuGroup;

    ///used to adecuat group menu
    void                   costumizeMenuGroup(QString name, corre::Project);
public:
    ///constructor: the standart sed in Qt
    explicit               tree(corre::Event *event, QWidget *parent = 0);
public slots:
    ///used for add a doc item. It is compatible with the event manager
    corre::Event::docData  addItem(corre::Document doc);
    ///used for add a document. It is compatible with the event manager
    void                   addItem(corre::Project pro);
    ///update the proyect data in the widget
    void                   updateProyect(corre::Project pro);
    ///update the document data in the widget
    void                   updateDocument(corre::Document doc);
    ///
    void                   closeDoc(corre::Document doc);
    ///
    void                   changeLangnuage(corre::Project pro);
private slots:
    ///it is invoqued when a action is clicked on the group menu.
    /// It make that this action is realiced in the correct way. Often
    /// it interactue with the event manager
    void                      doMenuGroup(QAction *option);
    ///Invoqued when a item is clicked. With the help of the
    /// mousePressEvent invoque the menu
    void                      itemClick(QTreeWidgetItem *item, int);

    //no necesary in this widget
    corre::Document           createDocByItem(DOC doc);
    void                      loadDoc(corre::Document doc);
    void                      unloadDoc(corre::Document doc);
};

}



#endif // TREE_H
